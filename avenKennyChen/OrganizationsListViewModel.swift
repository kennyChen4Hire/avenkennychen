//
//  ViewModel.swift
//  avenKennyChen
//
//  Created by Kenny Chen on 8/31/21.
//

import Foundation
import Combine

class OrganizationsListViewModel: Listable {
    
    typealias T = Organization

    var items = CurrentValueSubject<[Organization], Never>([])
    
    var numOfRows: Int {
        items.value.count
    }
    
    var numOfSections: Int {
        1
    }
    
    var selectedIndexPath: IndexPath? 

    var cancellables = Set<AnyCancellable>()
    
    init() {}
    
    func bind() {
        T.fetch(with: .all(10))
            .replaceError(with: [])
            .assign(to: \.value, on: items)
            .store(in: &cancellables)
    }
}

protocol Listable {
    associatedtype T: Codable
    var items: CurrentValueSubject<[T], Never> { get }
    var numOfRows: Int { get }
    var numOfSections: Int { get }
}
