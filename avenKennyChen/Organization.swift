//
//  Organization.swift
//  avenKennyChen
//
//  Created by Kenny Chen on 8/31/21.
//

import Foundation
import Combine

struct Organization: Codable {
    let name: String
    let url: String
    let avatarUrl: String
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "login"
        case url
        case avatarUrl = "avatar_url"
        case description
    }
    
    static func fetch(with request: OrganizationRequest) -> AnyPublisher<[Organization], Error> {
        return URLSession.shared.dataTaskPublisher(for: request.request)
            .tryMap { data -> Data in
                guard let httpResponse = data.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                return data.data
            }
            .decode(type: [Organization].self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}

public enum OrganizationRequest: Request {
    case all(_ maxResults: Int)
    
    public var scheme: Scheme {
        .https
    }
    
    public var baseUrl: String {
        "api.github.com"
    }
    
    public var endpoint: Endpoint {
        .organizations
    }
    
    public var method: HTTPMethod {
        .get
    }
    
    public var parameters: [String: String] {
        switch self {
        case .all(let count):
            return ["per_page": "\(count)"]
        }
    }
    
    public var headers: [String : String] {
        ["Accept": "application/vnd.github.v3+json"]
    }
    
    public var dataType: DataType {
        .JSON
    }
    
    public var url: URL {
        var components = URLComponents()
        components.scheme = scheme.rawValue
        components.host = baseUrl
        components.path = endpoint.rawValue
        components.queryItems = parameters.compactMap { (key, value) in
           return URLQueryItem(name: key, value: value)
        }
        guard let url = components.url else { preconditionFailure("invalid url created in request")}
        return url
    }
    
    public var request: URLRequest {
        var networkRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
        networkRequest.allHTTPHeaderFields = headers
        networkRequest.httpMethod = method.rawValue
        return networkRequest
    }
}
