//
//  OrganizationTableViewCell.swift
//  avenKennyChen
//
//  Created by Kenny Chen on 8/31/21.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher
class OrganizationTableViewCell: UITableViewCell {
    static var identifier = "OrganizationCell"
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: 14)
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var orgUrlLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.textColor = .blue
        let tapGesture =
            UITapGestureRecognizer(target: self, action: #selector(didTapUrl))
        label.addGestureRecognizer(tapGesture)
        return label
    }()
    
    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var labels: [UILabel] {
        [titleLabel, orgUrlLabel, descriptionLabel]
    }
        
    override func prepareForReuse() {
        labels.forEach { $0.text = ""}
    }
    
    @objc func didTapUrl() {
        guard let labelText = self.orgUrlLabel.text else { preconditionFailure("invalid git url")}
        var components = URLComponents(string: labelText)
        components?.host = "github.com"
        
        if let url = components?.url?.absoluteURL {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func configure(with organization: Organization, selected: Bool) {
        self.titleLabel.text = organization.name
        self.descriptionLabel.text = organization.description
        self.orgUrlLabel.text = organization.url
        self.avatarImageView.kf.setImage(with: URL(string: organization.avatarUrl)!)
        self.accessoryType = selected ? .checkmark : .none
        self.layoutViews()
    }
    
    func layoutViews() {
        let stackView = UIStackView(arrangedSubviews: labels)
        stackView.alignment = .leading
        stackView.axis = .vertical
        stackView.distribution = .equalCentering
        
        self.contentView.addSubview(self.avatarImageView)
        self.contentView.addSubview(stackView)
        
        avatarImageView.snp.makeConstraints {
            $0.top.left.bottom.equalTo(self.contentView)
            $0.width.equalTo(100)
        }
        
        stackView.snp.makeConstraints {
            $0.top.bottom.right.equalTo(self.contentView)
            $0.left.equalTo(self.avatarImageView.snp.right)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
