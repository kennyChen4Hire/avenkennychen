//
//  NetworkManager.swift
//  avenKennyChen
//
//  Created by Kenny Chen on 8/31/21.
//

import Foundation
import Combine

public enum DataType {
    case JSON
    case Data
}

public protocol Request {
    var endpoint: Endpoint { get }
    var baseUrl: String { get }
    var method: HTTPMethod { get }
    var scheme: Scheme { get }
    var parameters: [String: String] { get }
    var url: URL { get }
    var headers: [String: String] { get }
    var request: URLRequest { get }
    var dataType: DataType { get }
}

public enum Endpoint: String {
    case organizations = "/organizations"
}

public enum Scheme: String {
    case http = "http"
    case https = "https"
}

public enum HTTPMethod: String {
    case post  = "POST"
    case put  = "PUT"
    case get  = "GET"
    case delete = "DELETE"
    case patch  = "PATCH"
}

