//
//  ViewController.swift
//  avenKennyChen
//
//  Created by Kenny Chen on 8/31/21.
//

import UIKit
import Combine
import SnapKit

class OrganizationsViewController: UIViewController {
    var viewModel = OrganizationsListViewModel()
    var tableView = UITableView()
    var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(OrganizationTableViewCell.self, forCellReuseIdentifier: OrganizationTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        configureTableView()
        self.viewModel.bind()
        self.viewModel.items
            .dropFirst()
            .sink {
                [weak self] _ in self?.tableView.reloadData()
            }
            .store(in: &cancellables)
    }
    
    func configureTableView() {
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

extension OrganizationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numOfRows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OrganizationTableViewCell.identifier, for: indexPath) as? OrganizationTableViewCell else { fatalError("erroneous cell instantiated")}
        cell.configure(with: viewModel.items.value[indexPath.row], selected: self.viewModel.selectedIndexPath == indexPath )
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let previous = self.viewModel.selectedIndexPath {
            self.tableView.deselectRow(at: previous, animated: true)
            let cell = tableView.cellForRow(at: previous)
            cell?.accessoryType = .none
        }
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        self.viewModel.selectedIndexPath = indexPath
        self.tableView.reloadData()
    }
}

